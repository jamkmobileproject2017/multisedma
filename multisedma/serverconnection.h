#ifndef SERVERCONNECTION_H
#define SERVERCONNECTION_H

#include "player.h"

#if defined(__ANDROID__)
#include <sys/select.h>
#endif
#include <arpa/inet.h>
#include <cstring>
#include <exception>
#include <functional>
#include <string>
#include <sys/time.h>
#include <vector>

namespace sv {

using On_connect = std::function<void(const Player_identity& id)>;

class Connection_error : public std::exception {
private:
  std::string msg;
  int error;

public:
  Connection_error() : error{0} {}

  Connection_error(const std::string& info, int error_code) : error{error_code}
  {
    msg = info;

    char error_message[128];
    error_message[127] = '\0';

    msg += ": ";
    msg += strerror_r(error_code, error_message, 127);
  }

  const char* what() const noexcept override { return msg.c_str(); }
  int get_errno() const { return error; }
};

class Connection {
public:
  using Identities = std::vector<Player_identity>;

  Connection(const int port);
  ~Connection();

  void wait_for_players(const int players, On_connect&& f);

  const Identities& identities() const { return m_identities; }

  void recv(const int sd, const ssize_t size, char* data) const;
  void send(const int sd, const ssize_t size, const char* data) const;
  void broadcast(const ssize_t size, const char* data) const;

  template <typename T> void recv(const Player_identity& pi, T& data) const
  {
    recv(pi.id, sizeof(T), reinterpret_cast<char*>(&data));
  }

  template <typename T>
  void send(const Player_identity& pi, const T& data) const
  {
    send(pi.id, sizeof(T), reinterpret_cast<const char*>(&data));
  }

  template <typename T> void broadcast(const T& data)
  {
    for (const Player_identity& pi : m_identities)
      send(pi, data);
  }

  template <typename T>
  void broadcast_except(const Player_identity& id, const T& data)
  {
    for (const Player_identity& identity : m_identities) {
      if (identity != id)
        send(identity, data);
    }
  }

private:
  void setup_connection(const int port);
  void add_player_identity(int sd, const On_connect& f);

public:
  constexpr static int max_clients = 4;

private:
  sockaddr_in m_addr;

  int m_master_sd;
  int m_client_sd[max_clients];
  fd_set m_readfds;

  Identities m_identities;
};

} // namespace sv

#endif // SERVERCONNECTION_H

#include "activitymanager.h"
#include <QDebug>
#include <QJSValue>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQmlProperty>
#include <QQuickItem>
#include <cassert>
#include <exception>

ActivityManager::ActivityManager(QQmlApplicationEngine& eng, QObject* parent)
    : QObject{parent}, m_engine(eng), m_activity{nullptr}
{
}

ActivityManager::~ActivityManager() { clearActivity(); }

void ActivityManager::loadScreen(QString url)
{
  m_activities.clear();
  clearActivity();
  m_engine.load(url);
  m_screenUrl = url;

  if (m_engine.rootObjects().isEmpty())
    throw std::runtime_error("No root object found in " + url.toStdString());
}

void ActivityManager::loadComponent(QString url)
{
  clearActivity();

  QQuickItem* parent =
      m_engine.rootObjects().first()->findChild<QQuickItem*>(s_parentComponent);

  QQmlComponent comp(&m_engine, url);
  if (!comp.isReady()) {
    throw std::runtime_error("An error occured while loading QML component: " +
                             comp.errorString().toStdString());
  }

  m_activity = qobject_cast<QQuickItem*>(comp.create());
  m_activity->setParentItem(parent);
  m_activity->setVisible(true);
}

void ActivityManager::clearActivity()
{
  if (m_activity) {
    m_activity->setVisible(false);
    m_activity->deleteLater();
    m_activity = nullptr;
  }
}

void ActivityManager::goToActivity(QString url, QJSValue data)
{
  assert(m_engine.rootObjects().size() == 1);

  m_activities.push_back({url, m_screenUrl, data.toVariant().toMap()});
  loadComponent(url);
}

void ActivityManager::goToActivityWithScreen(QString activityUrl,
                                             QString screenUrl, QJSValue data)
{
  loadScreen(screenUrl);
  goToActivity(activityUrl, data);
}

void ActivityManager::goBack(int distance)
{
  if (static_cast<unsigned>(distance) >= m_activities.size())
    return;

  while (distance-- > 0)
    m_activities.pop_back();

  const Activity& top = m_activities.back();

  switchScreen(top.screen);
  loadComponent(top.url);
}

QVariantMap ActivityManager::getData(int distance)
{
  return std::prev(m_activities.end(), distance + 1)->data;
}

void ActivityManager::setData(QString key, QVariant value, int dinstance)
{
  std::prev(m_activities.end(), dinstance + 1)->data[key] = value;
}

void ActivityManager::switchScreen(QString url)
{
  if (url != m_screenUrl)
    loadScreen(url);
}

import QtQuick 2.4
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import "."

Item {
    id: item1
    width: 400
    height: 400
    property alias tAddress: tAddress
    property alias tPort: tPort
    property alias btnJoin: btnJoin
    property alias tPassword: tPassword

    Text {
        id: text1
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: join_game_layout.top
        anchors.bottomMargin: 40
        text: qsTr("Join Game")
        color: Style.textColor
        font.family: Style.fontFamilyName
        font.pointSize: Style.titleFontSize
    }

    ColumnLayout{
        id:join_game_layout
        anchors.centerIn: parent
        spacing: 5

        TextField {
            id: tAddress
            text: "127.0.0.1"
            placeholderText: "Address"
            anchors.horizontalCenter: parent.horizontalCenter
        }

        TextField {
            id: tPort
            text: "8888"
            placeholderText: "Port"
            anchors.horizontalCenter: parent.horizontalCenter
        }

        TextField {
            id: tPassword
            text: ""
            echoMode: TextInput.Password
            placeholderText: "password"
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Button {
            id: btnJoin
            text: qsTr("Join")
            anchors.horizontalCenterOffset: 0
            anchors.horizontalCenter: parent.horizontalCenter
        }
    }
}

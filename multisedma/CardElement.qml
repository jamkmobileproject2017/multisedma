import QtQuick 2.0
import QtGraphicalEffects 1.0

Item{
    id: card_parent
    property int anch_anim_dur: 300
    z:cur_index
    property alias card_image: card_image
    property bool is_clickable: true
    signal reset
    signal highlight(var killing_indices)
    signal deal(var card_src)
    signal end()
    anchors.fill: parent

    onDeal: {
//        console.log(names_array[0] + "card cur_index " + cur_index+":" + card_src[cur_index])
        if(card_src[cur_index] === ""){
            is_clickable = false
//            card_mouse_area.enabled = false
            card_image.opacity = 0
        } else {
            card_image.source = getCardPath(card_src[cur_index])
        }
    }

    onEnd: {
        card_image.visible = false
        aurora.opacity = 1
        aurora_enemy.opacity = 1
    }

    Image {
        id: card_image
        width: player_card_width
        height: player_card_height
        rotation: hand_rotation_start + rotation_increment * cur_index

        anchors {
            horizontalCenter: parent.horizontalCenter
            horizontalCenterOffset: hand_offset_start + offset_increment * cur_index

            bottom: parent.bottom
            bottomMargin: bottom_margin
        }

        ColorOverlay {
            id:overlay
            anchors.fill: card_image
            source: card_image
            color: "transparent"
        }
    }

    MouseArea {
        id: card_mouse_area
        width: player_card_width
        height: player_card_height
        rotation: hand_rotation_start + rotation_increment * cur_index
        property bool is_clicked: false
        onClicked: {
            if(can_click){
                if(!is_clicked && is_clickable){
                    anch_anim_dur = 300
                    is_clicked = true
                    can_click = false
                    aurora.opacity = 0
                    aurora_enemy.opacity = 1
                    parent.state = "reanchored";
                    card_parent.z = z_index++;
                    game.layCard(cur_index)
                    if(game.is_decision_pass){
                        btn_pass_kill.visible = false
                        btn_pass_kill.enabled = false
                        game.is_decision_pass = false
                        game.passRound(false)
                        game_table.highlight(null)
                    }
                }
            } else {
                console.log("It is not your turn yet.")
            }
        }

        anchors {
            horizontalCenter: parent.horizontalCenter
            horizontalCenterOffset: hand_offset_start + offset_increment * cur_index

            bottom: parent.bottom
            bottomMargin: bottom_margin
        }
    }

    states: [
        State {
            name: "reanchored"
            PropertyChanges {
                target: card_image
                anchors{
                    horizontalCenterOffset: -card_width/2
                    verticalCenterOffset:0
                }
                rotation:Math.random() * (10 - (-10)) + (-10)
                width:player_card_width
                height:player_card_height
            }
            AnchorChanges{
                target: card_image

                anchors{
                    top: undefined
                    left: undefined
                    right: undefined
                    bottom: undefined
                }
                anchors.verticalCenter: card_parent.verticalCenter
                anchors.horizontalCenter: card_parent.horizontalCenter
            }
        }
    ]

    transitions: Transition{
        AnchorAnimation{
            duration: anch_anim_dur
        }
    }

    /**At the end of every round,
      *layed cards are returned to their default position
    */
    onReset: {
        anch_anim_dur = 80
        card_parent.state = ""
        card_mouse_area.is_clicked = false
        z: cur_index
    }

    /**when player can choose only particular cards
      *the other cards are highlighted red
    */
    onHighlight: {
        if(killing_indices){
            //if it is not killing card we colour it red
            if(!killing_indices[cur_index] && card_parent.state === ""){
                overlay.color = "#80800000"
                card_mouse_area.enabled = false
            }
        } else {  //if the array is empty we reset the colouring of this card to default
            overlay.color = "transparent"
            card_mouse_area.enabled = true
        }
    }
}

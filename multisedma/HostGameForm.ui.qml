import QtQuick 2.4
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import "."

Item {
    id: form_host_game
    anchors.fill: parent
    property alias inpt_session_name: input_session_name
    property alias spin_no_players: spin_no_players
    property alias input_password: input_password
    property alias chbx_unlimited_deck: chbx_unlimited_deck
    property alias btn_create: btn_create
    property alias input_session_name: input_session_name

    Text {
        id: txt_host_game
        anchors{
            bottom: host_game_layout.top
            bottomMargin: 40
            horizontalCenter: parent.horizontalCenter
        }
        text: qsTr("Host Game")
        font.family: Style.fontFamilyName
        font.pointSize: Style.titleFontSize
        color: Style.textColor
    }

    ColumnLayout {
        id: host_game_layout
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        spacing: 2

        Text {
            id: txt_session_name
            color: Style.textColor
            text: "Session name"
            font.bold: true
            font.pixelSize: Style.textFontSize
        }

        TextField {
            id: input_session_name
            KeyNavigation.tab: spin_no_players
            width: parent.width
            height: 29
            placeholderText: "New_session"
        }
        Text {
            id: txt_no_players
            color: Style.textColor
            text: "Number of players"
            font.bold: true
            font.pixelSize: Style.textFontSize
        }
        SpinBox {
            id: spin_no_players
            KeyNavigation.tab: input_password

            width: parent.width
            height: 29
            spacing: 15

            font.pointSize: 10
            value: 1 // TODO: only for testing purposes, set this to 2
            from: 1
            to: 4
        }
        Text {
            id: txt_passwd
            color: Style.textColor
            text: qsTr("Password")
            font.bold: true
            font.pixelSize: Style.textFontSize
        }
        TextField {
            id: input_password
            KeyNavigation.tab: chbx_unlimited_deck
            width: parent.width
            height: 29
            placeholderText: qsTr("Password")
            echoMode: TextField.Password
        }
        CheckBox {
            id: chbx_unlimited_deck
            KeyNavigation.tab: btn_create
            Label {
                y: parent.height / 8
                text: "Unlimited deck"
                font.pixelSize: Style.smallTextFontSize
                color: Style.textColor
                anchors.left: parent.right
            }
            anchors.horizontalCenterOffset: 1
            font.bold: true
            anchors.left: parent.left
            transformOrigin: Item.Center
        }
        Button {
            id: btn_create
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Create"
            font.pointSize: 12
        }
    }
}

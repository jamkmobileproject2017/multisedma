import QtQuick 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.0
import QtQuick.Particles 2.0
import "."

Item {
    id: root
    anchors.fill: parent
    width: 480
    height: 640
    property alias top_card_player_3: top_card_player_3
    property alias top_card_player_2: top_card_player_2
    property alias top_card_player_1: top_card_player_1
    property alias top_card_player_0: top_card_player_0
    property alias bottom_card_player_3: bottom_card_player_3
    property alias bottom_card_player_2: bottom_card_player_2
    property alias bottom_card_player_1: bottom_card_player_1
    property alias bottom_card_player_0: bottom_card_player_0
    property alias aurora: green_aurora
    property alias player_score: player_score
    property alias aurora_enemy: green_aurora_enemy
    property alias enemy_name: enemey_name
    property alias enemy_score: enemy_score
    property alias btn_pass_kill: btn_pass_kill
    property alias emitter: emitter
    property alias winner_name:winner_name
    property alias final_stats:final_stats

    //this variable determines how far from vertical/horizontal center of the parent will the first card of the hand appear
    property int hand_offset_start: -110
    //this variable determines the space between two neighbouring cards in the hand
    property int offset_increment: 70

    //this variable determines rotation of the first card in the hand
    property int hand_rotation_start: -30
    //this variable determines how much is next card rotated in respect to the previous one
    property int rotation_increment: 20

    //determines how far from the bottom will the player cards be placed
    property int bottom_margin: 40

    property int card_width: 70
    property int card_height: 122
    property int player_card_width: 80
    property int player_card_height: 140

    property int z_index: 0
    property bool can_click: false

    signal trigger_enemy(int card_index, string card_name)
    signal reset
    signal highlight(var killing_indices)
    signal deal(var card_src_arr, var enemy_cards)
    signal end()

    /*================== 1 ===================*/
    CardElement {
        id: bottom_card_player_0
        property int cur_index: 0
    }

    CardElement {
        id: bottom_card_player_1
        property int cur_index: 1
    }

    CardElement {
        id: bottom_card_player_2
        property int cur_index: 2
    }

    CardElement {
        id: bottom_card_player_3
        property int cur_index: 3
    }
    /*========================== 1 ==========================*/

    /*============================= 2 ========================*/
    CardEnemyElement{
        id: top_card_player_0
        property int cur_index: 0
    }

    CardEnemyElement {
        id: top_card_player_1
        property int cur_index: 1
    }

    CardEnemyElement {
        id: top_card_player_2
        property int cur_index: 2
    }

    CardEnemyElement {
        id: top_card_player_3
        property int cur_index: 3
    }
    /*============================= 2 ========================*/

    Image{
        id:green_aurora
        width: parent.width
        height: 50
        anchors{
            horizontalCenter: parent.horizontalCenter
            bottom: parent.bottom
        }
        opacity: 0
        z:-1
        fillMode: Image.TileHorizontally
        source: "qrc:assets/gradient_down.png"
    }

    Image{
        id:green_aurora_enemy
        height: 50
        width: parent.width
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        opacity: 0
        z:-1
        fillMode: Image.TileHorizontally
        source: "qrc:assets/gradient_up.png"
    }

    Text {
        id: enemey_name
        text: game_table.names_array[0] === undefined ? "player" : game_table.names_array[0]
        font.pointSize: 32
        font.family: Style.fontFamilyName
        color: Style.textColor
        anchors.top: parent.top
        anchors.topMargin: card_height + card_height/4
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Text {
        id: player_score
        text: "0"
        font.pointSize: 42
        font.family: Style.fontFamilyName
        color: "#2E4172"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        z:1000
        style: Text.Outline; styleColor: "#1FE700"
    }
    Text {
        id: enemy_score
        text: "0"
        font.pointSize: 42
        font.family: Style.fontFamilyName
        color: "#2E4172"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: card_height/4
        z:1000
        style: Text.Outline; styleColor: "#1FE700"
    }

    Button{
        id:btn_pass_kill
        anchors.left: player_score.right
        anchors.bottom: parent.bottom
        anchors.bottomMargin: player_card_height/8
        anchors.leftMargin: player_card_width + player_card_width/2
        visible: false
        enabled: false
        text: "Pass"

    }

    ParticleSystem {
        id: particleSystem
    }

    Emitter {
        id: emitter
        anchors.centerIn: parent
        width: 160; height: 80
        system: particleSystem
        emitRate: 50
        lifeSpan: 1000
        lifeSpanVariation: 500
        size:40
        enabled:false

        velocity: AngleDirection{
            angle: 0
            angleVariation: 180
            magnitude: 300
        }
    }

    ImageParticle {
        source: "assets/cards/backSide.png"
        width: player_card_width; height: player_card_height
        system: particleSystem
        rotation: 80
        rotationVariation: 50
        rotationVelocity: 500
        z: 100
    }

    Text {
        id: winner_name
        text: ""
        anchors.centerIn: parent
        visible: false
        font.pointSize: Style.titleFontSize
        color: Style.textColor
        font.bold: true
    }

    Rectangle{
        id:final_stats
        color: "white"
        opacity: 0.5
        anchors.fill: parent
        anchors.margins: 10
        z:1001
        radius: 10
        visible: false
        ColumnLayout{
            anchors.left: parent.left
            anchors.leftMargin: 15
            anchors.top: parent.top
            anchors.topMargin: 15

            RowLayout{

                Text{
                    text: "1. "
                }

                Text{
                    text: myName
                }
                Text {
                    text: myScore
                }
            }
            RowLayout{
                Text{
                    text: "2. "
                }

                Text{
                    text: enName
                }

                Text{
                    text: enScore
                }
            }
        }
    }
}

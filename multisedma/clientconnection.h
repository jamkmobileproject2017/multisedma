#ifndef CLIENTCONNECTION_H
#define CLIENTCONNECTION_H

#include "player.h"
#include <cstring>
#include <string>

namespace cl {

class Connection_error : public std::exception {
private:
  std::string msg;
  int error;

public:
  Connection_error() : error{0} {}

  Connection_error(const std::string& info, int error_code) : error{error_code}
  {
    msg = info;

    char error_message[128];
    error_message[127] = '\0';

    msg += ": ";
    msg += strerror_r(error_code, error_message, 127);
  }

  const char* what() const noexcept override { return msg.c_str(); }
  int get_errno() const { return error; }
};

class Connection {
public:
  Connection(const char* address, int port, const char* player_name);
  ~Connection();

  void recv(const ssize_t size, char* data) const;
  void send(const ssize_t size, const char* data) const;

  template <typename T> void recv(T& data) const
  {
    recv(sizeof(T), reinterpret_cast<char*>(&data));
  }

  template <typename T> void send(const T& data) const
  {
    send(sizeof(T), reinterpret_cast<const char*>(&data));
  }

private:
  void connect_to_server(const char* address, int port);
  void send_identity(const char* player_name);

private:
  int m_sd;
};

} // namespace cl

#endif // CLIENTCONNECTION_H

#include "client.h"
#include "player.h"

#include <QDebug>
#include <cassert>
#include <iostream>
using namespace std;

void Client::gameInitSync() { m_barrier.set_value(); }

void Client::joinGame()
{
  m_thread = std::thread([this] {
    m_conn = std::make_unique<cl::Connection>(
        m_address.toStdString().c_str(), m_port,
        m_playerName.toStdString().c_str());

    synchronize();

    // We're ready
    emit gameReady();

    // Wait for Game
    m_barrier.get_future().get();

    recvIdentities();

    playGame();
  });
}

void Client::playGame()
{
  MessageType mt;
  m_conn->recv(mt);

  for (; mt != MessageType::endGame; m_conn->recv(mt)) {
    recvPlayerData();
    playRound();
    emit endOfRound();
  }

  int id;
  m_conn->recv(id);
  emit gameEnded(id);
}

void Client::playRound()
{
  for (MessageType mt = MessageType::none; mt != MessageType::endPlayRound;
       m_conn->recv(mt)) {
    servePlayer();
  }
}

void Client::servePlayer()
{
  int id;
  m_conn->recv(id);

  emit playersTurn(id);

  MessageType mt;
  m_conn->recv(mt);
  assert(mt == MessageType::finish || mt == MessageType::play ||
         mt == MessageType::kill);

  if (mt == MessageType::finish) {
    ScoreResult sr;
    m_conn->recv(sr);
    emit roundWon(sr.id, sr.score);
  }
  else if (mt == MessageType::play) {
    if (id == m_playerID) {
      LaidCard c = waitForCard();
      m_conn->send(c);
    }
    else {
      LaidCard c;
      m_conn->recv(c);
      emit playerLaidCard(c.player_id, c.index, cardName(c.card));
    }
  }
  else if (mt == MessageType::kill) {
    bool canKill;
    m_conn->recv(canKill);

    if (canKill) {
      bool willKill = !waitForPassRound(recvKillCards());
      m_conn->send(willKill);

      if (willKill) {
        LaidCard c = waitForCard();
        m_conn->send(c);
      }
    }
  }
}

const QList<int>& Client::playerIDs() const { return m_enemyIDs; }

void Client::layCard(int index)
{
  LaidCard c;
  c.index = index;
  c.player_id = m_playerID;
  c.card = m_playerData.cards[index];

  m_laidCard.set_value(c);
}

void Client::passRound(bool pass) { m_passRound.set_value(pass); }

void Client::synchronize()
{
  bool ready;
  m_conn->recv(ready);
}

void Client::recvIdentities()
{
  // Receive the number of players

  int nplayers;
  m_conn->recv(nplayers);

  // Receive their identities

  for (int i = 0; i < nplayers; i++) {
    Player_identity id;

    m_conn->recv(id);
    m_identities.insert(id.id, id.name);
    m_playerScores[id.id] = 0;
  }

  m_conn->recv(m_playerID);
  m_playerScores[m_playerID] = 0;

  for (auto it = m_identities.begin(); it != m_identities.end(); ++it) {
    if (it.key() != m_playerID) {
      m_enemyNames << it.value();
      m_enemyIDs << it.key();
    }
  }
}

void Client::recvPlayerData()
{
  m_conn->recv(m_playerData);

  QStringList atHand;
  for (int i = 0; i < Player_data::max_cards_at_hand; i++) {
    if (m_playerData.cards[i] == Card::none)
      atHand << "";
    else
      atHand << cardName(m_playerData.cards[i]);
  }

  int enemyID;
  m_conn->recv(enemyID);

  QList<bool> enemyCards;
  bool b;

  for (int i = 0; i < Player_data::max_cards_at_hand; i++) {
    m_conn->recv(b);
    enemyCards << b;
  }

  emit dealCards(atHand, enemyCards);
}

QList<bool> Client::recvKillCards()
{
  QList<bool> cards;
  bool b;

  for (int i = 0; i < Player_data::max_cards_at_hand; i++) {
    m_conn->recv(b);
    cards << b;
  }

  return cards;
}

LaidCard Client::waitForCard()
{
  emit cardRequest();
  LaidCard c = m_laidCard.get_future().get();
  m_playerData.layCard(c);

  m_laidCard = std::promise<LaidCard>();

  return c;
}

bool Client::waitForPassRound(const QList<bool>& killCards)
{
  emit playerPassDecision(killCards);
  bool p = m_passRound.get_future().get();

  m_passRound = std::promise<bool>();

  return p;
}

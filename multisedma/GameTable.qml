import QtQuick 2.4
import multisedma.Game 1.0

GameTableForm {
    id:game_table
    anchors.fill: parent

    function getCardPath(cardName) {
        return "qrc:assets/cards/" + cardName + ".png"
    }

    property var names_array
    property var myId
    property var myName
    property var myScore
    property var enId
    property var enName
    property var enScore


    Game {
        id:game

        property bool is_decision_pass: false

        onDealCards: {
            names_array = playerNames()
            myId = playerID()
            myName = playerName(myId)
            deal(playerCards, enemyCards)
//            console.log(names_array[0] + " has cards: "+playerCards)
        }

        //highlights the player that is now on turn
        onPlayersTurn: {
            if (id === playerID()){
                console.log("It's your turn")
                can_click = true
                aurora.opacity = 100
                aurora_enemy.opacity = 0
            }
            else{
                console.log("It's " + playerName(id) + "'s turn") //vracia meno hraca podla ID
                enId = id
                enName = playerName(id)
                can_click = false
                aurora.opacity = 0
                aurora_enemy.opacity = 100
            }
        }

        //server is waiting for a card
        onCardRequest: {
            console.log("Lay a card!")
        }

        //the other player just laid his card
        onPlayerLaidCard: {
            //id of the player that laid the card
            var name = playerName(id)
            console.log("Player " + name + " laid " + card + " (index " + index + ")")
            trigger_enemy(index,card)

        }

        onEndOfRound: reset()

        //when player can decide whether he will kill or pass,
        //the possible cards get highlighted
        onPlayerPassDecision: {
            is_decision_pass = true
            btn_pass_kill.visible = true
            btn_pass_kill.enabled = true
            highlight(killCards)

        }

        onRoundWon: {
            if(playerID() === id){
                player_score.text = score
                myScore = score
            } else {
                enemy_score.text = score
                enScore = score
            }
            emitter.enabled = true
            timer.start()
        }

        onGameEnded: {
            end()
            if(winnerID === playerID()){
                winner_name.visible = true
                winner_name.text = "You are winner!"
            }else {
                winner_name.text = "Winner is " + playerName(winnerID)
                winner_name.visible = true
            }
        }
    }

    //when player does not want to kill the round
    btn_pass_kill.onClicked: {
        game.passRound(true)
        btn_pass_kill.visible = false
        btn_pass_kill.enabled =false
        game.is_decision_pass = false
        highlight(null)
    }

    //sends signal to the card that should be moved, acording to enemy laid cards
    onTrigger_enemy: {
        switch(card_index){
        case 0: top_card_player_0.trigger(card_index, card_name); break
        case 1: top_card_player_1.trigger(card_index, card_name); break
        case 2: top_card_player_2.trigger(card_index, card_name); break
        case 3: top_card_player_3.trigger(card_index, card_name); break
        default: console.log("Card index " + card_index + " does not match any card.")
        }
    }

    //returns the laid cards back to its default position
    onReset: {
        z_index = 0
        can_click = false
        bottom_card_player_0.reset()
        bottom_card_player_1.reset()
        bottom_card_player_2.reset()
        bottom_card_player_3.reset()
        top_card_player_0.reset()
        top_card_player_1.reset()
        top_card_player_2.reset()
        top_card_player_3.reset()
    }

    onHighlight: {
        bottom_card_player_0.highlight(killing_indices)
        bottom_card_player_1.highlight(killing_indices)
        bottom_card_player_2.highlight(killing_indices)
        bottom_card_player_3.highlight(killing_indices)
    }

    onDeal: {
        bottom_card_player_0.deal(card_src_arr)
        bottom_card_player_1.deal(card_src_arr)
        bottom_card_player_2.deal(card_src_arr)
        bottom_card_player_3.deal(card_src_arr)

        top_card_player_0.deal(enemy_cards)
        top_card_player_1.deal(enemy_cards)
        top_card_player_2.deal(enemy_cards)
        top_card_player_3.deal(enemy_cards)
    }

    onEnd: {
        timer.interval = 5000
        emitter.enabled = true
        final_stats.visible = true
        bottom_card_player_0.end()
        bottom_card_player_1.end()
        bottom_card_player_2.end()
        bottom_card_player_3.end()
        top_card_player_0.end()
        top_card_player_1.end()
        top_card_player_2.end()
        top_card_player_3.end()
    }

    Timer{
        id:timer
        interval: 500
        running: false
        onTriggered: emitter.enabled = false
    }
}

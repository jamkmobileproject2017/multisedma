#ifndef ACTIVITYMANAGER_H
#define ACTIVITYMANAGER_H

#include <QJSValue>
#include <QObject>
#include <QQuickItem>
#include <QVariant>
#include <vector>

class QQmlApplicationEngine;
class QQuickItem;

/**
 * Screen is the main display container and may contain an Activity.
 * In order to display an Activity in the Screen, the user needs to
 * create an Item with its objectName set to "componentView":
 *
 * Item {
 *   objectName: "componentView"
 *   anchors.fill: parent
 * }
 *
 * By calling ActivityManager.goToActivity("some URL"), ActivityManager
 * will look for "componentView" Item and insert the Activity as a child
 * of the Item.
 *
 * Arbitrary data objects (that are supported by QVariant) may be passed
 * to new activities like so:
 *
 * property var someData: ({key1: "value1", key2: 1, key3: true})
 * ...
 * ActivityManager.goToActivity("some URL", someData)
 *
 * The data object may be then retrieved from the new activity:
 * var data = ActivityManager.getData()
 * console.log(data.key1)
 */

class ActivityManager : public QObject {
  Q_OBJECT

  constexpr static const char* s_parentComponent = "componentView";

  struct Activity {
    QString url;
    QString screen;
    QVariantMap data;
  };

  QQmlApplicationEngine& m_engine;
  std::vector<Activity> m_activities;

  QString m_screenUrl;
  QQuickItem* m_activity;

public:
  explicit ActivityManager(QQmlApplicationEngine& eng,
                           QObject* parent = nullptr);
  ~ActivityManager();

  // Loads a new screen and sets it as the default screen for all new Activities
  Q_INVOKABLE void loadScreen(QString url);

  Q_INVOKABLE void goToActivity(QString url, QJSValue data = QJSValue());

  // Loads a new screen and an activity in a single step
  Q_INVOKABLE void goToActivityWithScreen(QString activityUrl,
                                          QString screenUrl,
                                          QJSValue data = QJSValue());

  // Goes back by `distance' activities (ie. the previous one is 1)
  Q_INVOKABLE void goBack(int distance = 1);

  Q_INVOKABLE QVariantMap getData(int distance = 0);
  Q_INVOKABLE void setData(QString key, QVariant value, int dinstance = 0);

private:
  void switchScreen(QString url);
  void loadComponent(QString url);
  void clearActivity();

signals:

public slots:
};

#endif // ACTIVITYMANAGER_H

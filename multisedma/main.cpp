#include "activitymanager.h"
#include "game.h"
#include "lobbymodel.h"
#include "session.h"
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

int main(int argc, char* argv[])
{
  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
  QGuiApplication app(argc, argv);

  qmlRegisterType<Session>("multisedma.Session", 1, 0, "Session");
  qmlRegisterType<Game>("multisedma.Game", 1, 0, "Game");
  qmlRegisterUncreatableType<LobbyModel>("multisedma.LobbyModel", 1, 0,
                                         "LobbyModel",
                                         "Cannot create LobbyModel from QML");

  QQmlApplicationEngine engine;
  ActivityManager activityManager{engine};
  engine.rootContext()->setContextProperty("ActivityManager", &activityManager);

  activityManager.loadScreen("qrc:/main.qml");
  activityManager.goToActivity("qrc:/ScrMenu.qml");

  return app.exec();
}

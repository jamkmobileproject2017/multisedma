#include "game.h"
#include "session.h"
#include <QDebug>

Game::Game(QObject* parent)
    : QObject(parent), sv{Session::server()}, cl{Session::client()}
{
  connect(cl, &Client::dealCards,
          [this](QStringList playerCards, QList<bool> enemyCards) {
            emit dealCards(playerCards, enemyCards);
          });

  connect(cl, &Client::cardRequest, [this] { emit cardRequest(); });
  connect(cl, &Client::playerPassDecision, [this](QList<bool> killCards) {
    emit playerPassDecision(killCards);
  });
  connect(cl, &Client::playersTurn, [this](int id) { emit playersTurn(id); });
  connect(cl, &Client::playerLaidCard, [this](int id, int index, QString card) {
    emit playerLaidCard(id, index, card);
  });

  connect(cl, &Client::endOfRound, [this] { emit endOfRound(); });
  connect(cl, &Client::roundWon,
          [this](int id, int score) { emit roundWon(id, score); });
  connect(cl, &Client::gameEnded,
          [this](int winnerID) { emit gameEnded(winnerID); });

  // Second phase of synchronization
  cl->connection()->send(bool{true});
  cl->gameInitSync();
}

QString Game::playerName(int id) const { return cl->identities()[id]; }

QString Game::tokenCard() const { return m_tokenCard; }

void Game::setTokenCard(QString card)
{
  m_tokenCard = card;
  emit tokenCardChanged();
}

int Game::playerID() { return cl->id(); }

QList<int> Game::playerIDs() { return cl->playerIDs(); }

QStringList Game::playerNames() { return cl->playerNames(); }

void Game::layCard(int index) { cl->layCard(index); }

void Game::passRound(bool pass) { cl->passRound(pass); }

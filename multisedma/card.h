#ifndef CARD_H
#define CARD_H

#include <vector>

enum class CardGroup {
  ace = 0x0010,
  IX = 0x0020,
  king = 0x0040,
  over_jack = 0x0080,
  under_jack = 0x0100,
  VII = 0x0200,
  VIII = 0x0400,
  X = 0x0800
};

constexpr int CardGroupMask = 0xFFF0;

constexpr inline int operator|(CardGroup cg, int flag)
{
  return static_cast<int>(cg) | flag;
}

enum class Card {
  none = -1,

  ace_acorns = CardGroup::ace | 0x0000,
  ace_bells = CardGroup::ace | 0x0001,
  ace_hearts = CardGroup::ace | 0x0002,
  ace_leaves = CardGroup::ace | 0x0003,

  IX_acorns = CardGroup::IX | 0x0000,
  IX_bells = CardGroup::IX | 0x0001,
  IX_hearts = CardGroup::IX | 0x0002,
  IX_leaves = CardGroup::IX | 0x0003,

  king_acorns = CardGroup::king | 0x0000,
  king_bells = CardGroup::king | 0x0001,
  king_hearts = CardGroup::king | 0x0002,
  king_leaves = CardGroup::king | 0x0003,

  over_jack_acorns = CardGroup::over_jack | 0x0000,
  over_jack_bells = CardGroup::over_jack | 0x0001,
  over_jack_hearts = CardGroup::over_jack | 0x0002,
  over_jack_leaves = CardGroup::over_jack | 0x0003,

  under_jack_acorns = CardGroup::under_jack | 0x0000,
  under_jack_bells = CardGroup::under_jack | 0x0001,
  under_jack_hearts = CardGroup::under_jack | 0x0002,
  under_jack_leaves = CardGroup::under_jack | 0x0003,

  VII_acorns = CardGroup::VII | 0x0000,
  VII_bells = CardGroup::VII | 0x0001,
  VII_hearts = CardGroup::VII | 0x0002,
  VII_leaves = CardGroup::VII | 0x0003,

  VIII_acorns = CardGroup::VIII | 0x0000,
  VIII_bells = CardGroup::VIII | 0x0001,
  VIII_hearts = CardGroup::VIII | 0x0002,
  VIII_leaves = CardGroup::VIII | 0x0003,

  X_acorns = CardGroup::X | 0x0000,
  X_bells = CardGroup::X | 0x0001,
  X_hearts = CardGroup::X | 0x0002,
  X_leaves = CardGroup::X | 0x0003,

  after_last_card = 0xFFFF
};

constexpr inline CardGroup getCardGroup(Card c)
{
  return static_cast<CardGroup>(static_cast<int>(c) & CardGroupMask);
}

using Deck = std::vector<Card>;

Deck createDeck();

enum class CardResult { none, ranked, kill };

CardResult resolveCard(Card token, Card laid);
bool isRanked(Card c);

const char* cardName(Card c);

struct LaidCard {
  Card card;
  int index;
  int player_id;
};

#endif // CARD_H

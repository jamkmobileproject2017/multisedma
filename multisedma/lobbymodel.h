#ifndef LOBBYMODEL_H
#define LOBBYMODEL_H

#include <QAbstractListModel>
#include <QList>

class LobbyModel : public QAbstractListModel {
  Q_OBJECT

  QList<QString> m_players;

public:
  LobbyModel(QObject* parent = nullptr) : QAbstractListModel{parent} {}

  int rowCount(const QModelIndex& parent = QModelIndex()) const override;
  QVariant data(const QModelIndex& index,
                int role = Qt::DisplayRole) const override;

public slots:
  void playerAdded(QString name);
};

#endif // LOBBYMODEL_H

#ifndef GAME_H
#define GAME_H

#include <QMap>
#include <QObject>

class Server;
class Client;

class Game : public QObject {
  Q_OBJECT

  // clang-format off
  Q_PROPERTY(QString tokenCard READ tokenCard WRITE setTokenCard NOTIFY tokenCardChanged)
  // clang-format on

  Server* sv;
  Client* cl;

  QString m_tokenCard;

public:
  explicit Game(QObject* parent = nullptr);

  Q_INVOKABLE QString playerName(int id) const;

  QString tokenCard() const;
  void setTokenCard(QString card);

  Q_INVOKABLE int playerID();
  Q_INVOKABLE QList<int> playerIDs();
  Q_INVOKABLE QStringList playerNames();
  Q_INVOKABLE void layCard(int index);
  Q_INVOKABLE void passRound(bool pass);

signals:
  void dealCards(QStringList playerCards, QList<bool> enemyCards);
  void cardRequest();
  void playerPassDecision(QList<bool> killCards);
  void playersTurn(int id);
  void playerLaidCard(int id, int index, QString card);

  void endOfRound();
  void roundWon(int id, int score);
  void gameEnded(int winnerID);

  void tokenCardChanged();

public slots:
};

#endif // GAME_H

#ifndef SESSION_H
#define SESSION_H

#include "client.h"
#include "server.h"
#include <QObject>
#include <memory>

class Session : public QObject {
  Q_OBJECT

  Q_PROPERTY(int playerCount READ playerCount WRITE setPlayerCount)
  Q_PROPERTY(QString address READ address WRITE setAddress)
  Q_PROPERTY(int port READ port WRITE setPort)
  Q_PROPERTY(QString playerName READ playerName WRITE setPlayerName)

  static std::unique_ptr<Server> s_sv;
  static std::unique_ptr<Client> s_cl;

public:
  explicit Session(QObject* parent = nullptr);

  static Server* server() { return s_sv.get(); }
  static Client* client() { return s_cl.get(); }
  Q_INVOKABLE LobbyModel* lobbyModel() const { return s_sv->lobbyModel(); }

  Q_INVOKABLE void hostGame() { s_sv->hostGame(); }
  Q_INVOKABLE void joinGame() { s_cl->joinGame(); }

  int playerCount() const { return s_sv->players(); }
  void setPlayerCount(int n) { s_sv->setPlayers(n); }

  QString address() const { return s_cl->address(); }
  void setAddress(QString addr) { s_cl->setAddress(addr); }

  int port() const { return s_cl->port(); }
  void setPort(int port) { s_cl->setPort(port); }

  QString playerName() const { return s_cl->playerName(); }
  void setPlayerName(QString name) { s_cl->setPlayerName(name); }

signals:
  void playerConnected(QString name);
  void serverReady();
  void gameJoined();
  void gameReady();
};

#endif // SESSION_H

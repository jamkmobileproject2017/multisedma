import QtQuick 2.4
import multisedma.Session 1.0
import multisedma.LobbyModel 1.0

HostGameWaitRoomForm {
    property var waitRoomData: ActivityManager.getData()

    txt_session_name.text: waitRoomData.session_name

    Session {
        id: ss

        playerCount: waitRoomData.no_players
        address: "127.0.0.1"
        port: 8888
        playerName: "player1"

        onServerReady: joinGame()

        onGameReady: {
            console.log("GAME READY")
            ActivityManager.goToActivity("qrc:/ScrGameTable.qml")
        }

        onPlayerConnected: console.log("Player '" + name + "' connected")
        onGameJoined: console.log("host joined")
    }

    Component.onCompleted: {
        ss.hostGame()
        btn_play.enabled = true // XXX only for testing purposes
    }

    listView.model: ss.lobbyModel()

    btn_play.onClicked: {
        ActivityManager.goToActivity("qrc:/ScrGameTable.qml")
    }
}

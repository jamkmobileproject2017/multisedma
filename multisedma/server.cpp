#include "server.h"
#include <algorithm>
#include <chrono>
#include <random>

#include <QDebug>

namespace {

bool hasEmptyHand(const Player_data& pd) { return pd.at_hand == 0; }

} // unnamed-namespace

Server::Server()
    : QObject(nullptr), m_players{}, m_lobbyModel(new LobbyModel(this))
{
  connect(this, &Server::playerConnected, m_lobbyModel,
          &LobbyModel::playerAdded);
}

void Server::hostGame()
{
  m_thread = std::thread([this] {
    m_conn = std::make_unique<sv::Connection>(8888);
    emit serverReady();

    m_conn->wait_for_players(m_players, [this](const Player_identity& id) {
      emit playerConnected(id.name);
    });

    synchronize();
    broadcastIdentities();

    playGame();
  });
}

void Server::synchronize()
{
  // First phase - broadcast bool{true}
  bool ready = true;
  m_conn->broadcast(ready);

  // Second phase - receive bool from all players
  for (const Player_identity& id : m_conn->identities()) {
    m_conn->recv(id, ready);
  }
}

void Server::playGame()
{
  // Create the deck

  Deck deck = createDeck();
  //  Deck deck(14, Card::VII_acorns);

  // Choose a starting player

  m_origin_player = m_conn->identities().begin();
  m_tokenPlayer = m_currentPlayer = m_origin_player;

  // Play the game

  for (;;) {
    dealCards(deck);

    if (std::any_of(m_player_data.begin(), m_player_data.end(),
                    [](auto kv) { return hasEmptyHand(kv.second); })) {
      break;
    }

    broadcastMessageType(MessageType::dealCards);
    sendPlayerData();
    sendEnemyData();

    m_origin_player = m_tokenPlayer;
    m_currentPlayer = m_origin_player;

    playRound();
  }

  broadcastMessageType(MessageType::endGame);
  m_conn->broadcast(m_tokenPlayer->id);
}

void Server::finishRound(int score)
{
  broadcastMessageType(MessageType::finish);
  broadcastScoreResult(score);
}

void Server::playRound()
{
  int score = 0;

  // Set a token card

  broadcastCurrentPlayer();
  broadcastMessageType(MessageType::play);

  LaidCard tokenCard = layCard();
  if (isRanked(tokenCard.card))
    score++;

  for (;;) {
    std::this_thread::sleep_for(std::chrono::seconds{1});

    broadcastMessageType(MessageType::none);
    circPlayer();

    if (hasEmptyHand(m_player_data[m_currentPlayer->id])) {
      finishRound(score);
      break;
    }

    if (m_currentPlayer == m_origin_player) {
      // We're back at the starting player

      m_conn->send(*m_currentPlayer, MessageType::kill);

      bool canKill = hasKillingCard(tokenCard.card);
      m_conn->send(*m_currentPlayer, canKill);

      if (canKill) {
        sendKillingCards(tokenCard.card);

        bool willKill;
        m_conn->recv(*m_currentPlayer, willKill);

        if (willKill) {
          m_conn->broadcast_except(*m_currentPlayer, MessageType::play);

          LaidCard c = layCard();
          CardResult res = resolveCard(tokenCard.card, c.card);

          if (isRanked(c.card))
            score++;

          if (res == CardResult::kill)
            m_tokenPlayer = m_currentPlayer;
        }
        else {
          sendDummyRound(*m_currentPlayer);
          finishRound(score);
          break;
        }
      }
      else {
        sendDummyRound(*m_currentPlayer);
        finishRound(score);
        break;
      }
    }
    else {
      broadcastMessageType(MessageType::play);

      LaidCard c = layCard();
      CardResult res = resolveCard(tokenCard.card, c.card);

      if (res == CardResult::kill)
        m_tokenPlayer = m_currentPlayer;

      if (isRanked(c.card))
        score++;
    }
  }

  broadcastMessageType(MessageType::endPlayRound);
}

void Server::dealCards(Deck& d)
{
  int maxCardsToDeal;
  if (d.size() < m_player_data.size() * Player_data::max_cards_at_hand)
    maxCardsToDeal = d.size() / m_player_data.size();
  else
    maxCardsToDeal = Player_data::max_cards_at_hand;

  for (const Player_identity& id : m_conn->identities()) {
    Player_data& pd = m_player_data[id.id];

    std::vector<Card> cardsAtHand;
    std::copy_if(std::begin(pd.cards), std::end(pd.cards),
                 std::back_inserter(cardsAtHand),
                 [](Card c) { return c != Card::none; });

    int cardsToDeal =
        std::min(Player_data::max_cards_at_hand - pd.at_hand, maxCardsToDeal);
    for (int i = 0; i < cardsToDeal && !d.empty(); i++) {
      cardsAtHand.push_back(d.back());
      d.pop_back();
      ++pd.at_hand;
    }

    std::fill(std::begin(pd.cards), std::end(pd.cards), Card::none);
    std::copy(cardsAtHand.begin(), cardsAtHand.end(), std::begin(pd.cards));
  }
}

void Server::broadcastIdentities()
{
  // Broadcast the number of players

  int nplayers = m_conn->identities().size();
  m_conn->broadcast(nplayers);

  // Broadcast the identities of all players

  for (const Player_identity& id : m_conn->identities())
    m_conn->broadcast(id);

  // Send each player their own ID

  for (const Player_identity& id : m_conn->identities())
    m_conn->send(id, id.id);
}

void Server::sendPlayerData()
{
  for (const Player_identity& id : m_conn->identities())
    m_conn->send(id, m_player_data[id.id]);
}

void Server::sendEnemyData()
{
  for (const Player_identity& id : m_conn->identities()) {
    for (const Player_identity& iid : m_conn->identities()) {
      if (id.id == iid.id)
        continue;

      m_conn->send(id, iid.id);
      for (Card c : m_player_data[iid.id].cards)
        m_conn->send(id, c != Card::none);
    }
  }
}

void Server::broadcastCurrentPlayer()
{
  m_conn->broadcast(m_currentPlayer->id);
}

void Server::broadcastMessageType(MessageType mt) { m_conn->broadcast(mt); }

void Server::broadcastScoreResult(int score)
{
  Player_data& pd = m_player_data[m_tokenPlayer->id];
  pd.score += score;

  ScoreResult sr;
  sr.id = m_tokenPlayer->id;
  sr.score = pd.score;

  m_conn->broadcast(sr);
}

void Server::broadcastDummyRound()
{
  broadcastMessageType(MessageType::none);
  m_conn->broadcast(int{-1});
}

void Server::sendDummyRound(const Player_identity& id)
{
  m_conn->send(id, MessageType::none);
  m_conn->send(id, int{-1});
}

void Server::circPlayer()
{
  if (++m_currentPlayer == m_conn->identities().end())
    m_currentPlayer = m_conn->identities().begin();

  broadcastCurrentPlayer();
}

LaidCard Server::layCard()
{
  LaidCard card;

  m_conn->recv(*m_currentPlayer, card);
  m_conn->broadcast_except(*m_currentPlayer, card);

  Player_data& pd = m_player_data[m_currentPlayer->id];
  pd.cards[card.index] = Card::none;
  pd.at_hand--;

  m_laidCards.push_back(card);

  return card;
}

bool Server::hasKillingCard(Card token_card)
{
  const Player_data& pd = m_player_data[m_origin_player->id];
  return std::any_of(std::begin(pd.cards), std::end(pd.cards),
                     [token_card](Card c) {
                       return resolveCard(token_card, c) == CardResult::kill;
                     });
}

void Server::sendKillingCards(Card tokenCard)
{
  const Player_data& pd = m_player_data[m_origin_player->id];
  std::array<bool, Player_data::max_cards_at_hand> cards;

  for (int i = 0; i < Player_data::max_cards_at_hand; i++)
    cards[i] = resolveCard(tokenCard, pd.cards[i]) == CardResult::kill;

  for (bool b : cards)
    m_conn->send(*m_currentPlayer, b);
}

#include "card.h"
#include <algorithm>
#include <array>
#include <random>

namespace {

const std::array<Card, 32> cards = {
    Card::ace_acorns,        Card::ace_bells,
    Card::ace_hearts,        Card::ace_leaves,

    Card::IX_acorns,         Card::IX_bells,
    Card::IX_hearts,         Card::IX_leaves,

    Card::king_acorns,       Card::king_bells,
    Card::king_hearts,       Card::king_leaves,

    Card::over_jack_acorns,  Card::over_jack_bells,
    Card::over_jack_hearts,  Card::over_jack_leaves,

    Card::under_jack_acorns, Card::under_jack_bells,
    Card::under_jack_hearts, Card::under_jack_leaves,

    Card::VII_acorns,        Card::VII_bells,
    Card::VII_hearts,        Card::VII_leaves,

    Card::VIII_acorns,       Card::VIII_bells,
    Card::VIII_hearts,       Card::VIII_leaves,

    Card::X_acorns,          Card::X_bells,
    Card::X_hearts,          Card::X_leaves};

} // unnamed-namespace

CardResult resolveCard(Card token, Card laid)
{
  const CardGroup cg = getCardGroup(laid);

  if (cg == getCardGroup(token))
    return CardResult::kill;

  if (cg == CardGroup::VII)
    return CardResult::kill;

  return CardResult::none;
}

const char* cardName(Card c)
{
  switch (c) {
  case Card::ace_acorns:
    return "ace_acorns";
  case Card::ace_bells:
    return "ace_bells";
  case Card::ace_hearts:
    return "ace_hearts";
  case Card::ace_leaves:
    return "ace_leaves";

  case Card::IX_acorns:
    return "IX_acorns";
  case Card::IX_bells:
    return "IX_bells";
  case Card::IX_hearts:
    return "IX_hearts";
  case Card::IX_leaves:
    return "IX_leaves";

  case Card::king_acorns:
    return "king_acorns";
  case Card::king_bells:
    return "king_bells";
  case Card::king_hearts:
    return "king_hearts";
  case Card::king_leaves:
    return "king_leaves";

  case Card::over_jack_acorns:
    return "over_jack_acorns";
  case Card::over_jack_bells:
    return "over_jack_bells";
  case Card::over_jack_hearts:
    return "over_jack_hearts";
  case Card::over_jack_leaves:
    return "over_jack_leaves";

  case Card::under_jack_acorns:
    return "under_jack_acorns";
  case Card::under_jack_bells:
    return "under_jack_bells";
  case Card::under_jack_hearts:
    return "under_jack_hearts";
  case Card::under_jack_leaves:
    return "under_jack_leaves";

  case Card::VII_acorns:
    return "VII_acorns";
  case Card::VII_bells:
    return "VII_bells";
  case Card::VII_hearts:
    return "VII_hearts";
  case Card::VII_leaves:
    return "VII_leaves";

  case Card::VIII_acorns:
    return "VIII_acorns";
  case Card::VIII_bells:
    return "VIII_bells";
  case Card::VIII_hearts:
    return "VIII_hearts";
  case Card::VIII_leaves:
    return "VIII_leaves";

  case Card::X_acorns:
    return "X_acorns";
  case Card::X_bells:
    return "X_bells";
  case Card::X_hearts:
    return "X_hearts";
  case Card::X_leaves:
    return "X_leaves";

  default:
    return "[ wrong card code ]";
  }
}

Deck createDeck()
{
  Deck d{cards.begin(), cards.end()};

  std::random_device rd;
  std::mt19937 g{rd()};
  std::shuffle(d.begin(), d.end(), g);

  return d;
}

bool isRanked(Card c)
{
  const CardGroup cg = getCardGroup(c);
  return cg == CardGroup::X || cg == CardGroup::ace;
}

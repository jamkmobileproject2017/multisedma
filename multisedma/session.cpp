#include "session.h"

std::unique_ptr<Server> Session::s_sv;
std::unique_ptr<Client> Session::s_cl;

Session::Session(QObject* parent) : QObject(parent)
{
  if (!s_sv)
    s_sv = std::make_unique<Server>();

  if (!s_cl)
    s_cl = std::make_unique<Client>();

  connect(s_sv.get(), &Server::playerConnected,
          [this](QString name) { emit playerConnected(name); });
  connect(s_sv.get(), &Server::serverReady, [this] { emit serverReady(); });

  connect(s_cl.get(), &Client::gameJoined, [this] { emit gameJoined(); });
  connect(s_cl.get(), &Client::gameReady, [this] { emit gameReady(); });
}

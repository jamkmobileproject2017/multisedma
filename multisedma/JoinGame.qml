import QtQuick 2.4
import multisedma.Session 1.0

JoinGameForm {
    Session {
        id: ss
        playerName: "player2"
        address: tAddress.text
        port: tPort.text

        onGameJoined: {
            console.log("GAME JOINED")
        }

        onGameReady: {
            ActivityManager.goToActivity("qrc:/ScrGameTable.qml")
        }
    }

    btnJoin.onClicked: {
        ss.joinGame()
        btnJoin.text = "Waiting for players..."
        btnJoin.enabled = false
    }
}

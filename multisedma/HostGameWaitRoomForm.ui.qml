import QtQuick 2.4
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import "."

Item {
    id: wait_room
    anchors.fill: parent

    property alias btn_play: btn_play
    property alias txt_session_name: txt_session_name
    property alias listView: listView

    Text {
        id: txt_wait_room
        text: "Waiting room"
        color: Style.textColor
        anchors.verticalCenter: parent.verticalCenter
        anchors.verticalCenterOffset: -150
        anchors.horizontalCenter: parent.horizontalCenter
        font.family: Style.fontFamilyName
        font.pointSize: Style.titleFontSize
    }

    Text {
        id: txt_session_name
        text: "Session name"
        anchors.top: txt_wait_room.bottom
        anchors.topMargin: 30
        anchors.horizontalCenter: parent.horizontalCenter
        font.family: Style.fontFamilyName
        font.pointSize: Style.textFontSize
        color: Style.textColor
    }


    ListView {
        height: parent.width
        anchors.top: txt_session_name.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.horizontalCenterOffset: -50
        anchors.topMargin: 50
        id: listView
        interactive: false
        spacing: 30

        delegate:Item{
            Row{
                Text {
                    text: (index + 1) + ". "
                    font.pointSize: Style.textFontSize
                    color: Style.textColor
                }
                Text {
                    id:txt
                    text: display
                    font.pointSize: Style.textFontSize
                    color: Style.textColor

                }
            }
        }
    }



    Button {
        id: btn_play
        anchors.top: txt_session_name.bottom
        anchors.topMargin: 190
        anchors.horizontalCenter: parent.horizontalCenter
        text: "Play"
        enabled: false
        font.pointSize: 12
    }
}

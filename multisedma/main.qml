import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

ApplicationWindow {
    id: root
    visible: true
    width: 480
    height: 640
    title: "MultiSedma"
    x: 600
    y: 300

    Shortcut {
        sequence: "Backspace"
        onActivated: ActivityManager.goBack()
    }

    Shortcut {
        sequence: "Back"
        onActivated: ActivityManager.goBack()
    }

    Image {
        id: bg
        source: "qrc:assets/bg3.jpg"
        width: 600
        height: 600
        fillMode: Image.Tile
        anchors.fill: parent
    }

    Item {
        objectName: "componentView"
        anchors.fill: parent
    }
    FontLoader { id: marioFont; source: "qrc:assets/SuperMario256.ttf" }
}

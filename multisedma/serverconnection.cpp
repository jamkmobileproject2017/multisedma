#include "serverconnection.h"
#include <cerrno>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

namespace sv {

Connection::Connection(const int port) : m_client_sd{}
{
  setup_connection(port);
}

Connection::~Connection()
{
  for (const Player_identity& pi : m_identities)
    close(pi.id);

  close(m_master_sd);
}

void Connection::setup_connection(const int port)
{
  m_master_sd = socket(AF_INET, SOCK_STREAM, 0);
  if (m_master_sd <= 0)
    throw Connection_error{"socket()", errno};

  const int opt = 1;
  if (setsockopt(m_master_sd, SOL_SOCKET, SO_REUSEADDR, (char*)&opt,
                 sizeof(opt)) < 0)
    throw Connection_error{"setsockopt()", errno};

  m_addr.sin_family = AF_INET;
  m_addr.sin_addr.s_addr = INADDR_ANY;
  m_addr.sin_port = htons(port);

  if (bind(m_master_sd, (sockaddr*)&m_addr, sizeof(m_addr)) < 0)
    throw Connection_error{"bind()", errno};

  if (listen(m_master_sd, max_clients) < 0)
    throw Connection_error{"listen()", errno};
}

void Connection::wait_for_players(const int players, On_connect&& f)
{
  const int addr_len = sizeof(m_addr);

  while (m_identities.size() != static_cast<unsigned>(players)) {
    // Clear the reading socket set
    FD_ZERO(&m_readfds);

    // Add master socket
    FD_SET(m_master_sd, &m_readfds);
    int max_sd = m_master_sd;

    // Add client sockets to the set
    for (int i = 0; i < max_clients; i++) {
      int sd = m_client_sd[i];

      if (sd > 0)
        FD_SET(sd, &m_readfds);

      if (sd > max_sd)
        max_sd = sd;
    }

    int active_sd = select(max_sd + 1, &m_readfds, nullptr, nullptr, nullptr);
    if (active_sd < 0 && errno != EINTR)
      throw Connection_error{"select()", errno};

    // Master socket is active - this means there's an incoming connection
    if (FD_ISSET(m_master_sd, &m_readfds)) {
      int new_sd =
          accept(m_master_sd, (sockaddr*)&m_addr, (socklen_t*)&addr_len);
      if (new_sd < 0)
        throw Connection_error{"accept()", errno};

      add_player_identity(new_sd, f);
    }
  }
}

void Connection::add_player_identity(int sd, const On_connect& f)
{
  int name_length;
  recv(Player_identity{sd, ""}, name_length);

  name_length = std::min(name_length, Player_identity::max_name_length - 1);

  char name[Player_identity::max_name_length];
  recv(sd, name_length, name);
  m_identities.emplace_back(sd, name);

  f(m_identities.back());
}

void Connection::recv(const int sd, const ssize_t size, char* data) const
{
  ssize_t n = 0;
  ssize_t bytes_left = size;

  errno = 0;
  while ((n = read(sd, data, bytes_left)) > 0) {
    data += n;
    bytes_left -= n;
  }

  if (errno != 0)
    throw Connection_error{"recv()", errno};
}

void Connection::send(const int sd, const ssize_t size, const char* data) const
{
  ssize_t n = 0;
  ssize_t bytes_left = size;

  errno = 0;
  while ((n = write(sd, data, bytes_left)) > 0) {
    data += n;
    bytes_left -= n;
  }

  if (errno != 0)
    throw Connection_error{"send()", errno};
}

void Connection::broadcast(const ssize_t size, const char* data) const
{
  for (const Player_identity& pi : m_identities)
    send(pi.id, size, data);
}

} // namespace sv

#include "lobbymodel.h"

int LobbyModel::rowCount(const QModelIndex&) const { return m_players.size(); }

QVariant LobbyModel::data(const QModelIndex& index, int role) const
{
  if (index.row() < 0 || index.row() >= m_players.count())
    return QVariant();

  if (role == Qt::DisplayRole)
    return m_players.at(index.row());

  return QVariant();
}

void LobbyModel::playerAdded(QString name)
{
  beginInsertRows(QModelIndex(), m_players.size(), m_players.size());
  m_players.append(name);
  endInsertRows();
}

import QtQuick 2.4
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import multisedma.Session 1.0

HostGameForm {

    property var hostGameFormData: ({
                                        session_name: inpt_session_name.text,
                                        no_players: spin_no_players.value,
                                        password: input_password.text,
                                        is_unlimited: chbx_unlimited_deck
                                    })

    btn_create.onClicked: {
        ActivityManager.goToActivity("qrc:/ScrHostGameWaitRoom.qml",
                                     hostGameFormData)
    }
}

#include "clientconnection.h"
#include <arpa/inet.h>
#include <cerrno>
#include <cstring>
#include <sys/socket.h>
#include <unistd.h>

namespace cl {

Connection::Connection(const char* address, int port, const char* player_name)
{
  connect_to_server(address, port);
  send_identity(player_name);
}

Connection::~Connection() { close(m_sd); }

void Connection::connect_to_server(const char* address, int port)
{
  m_sd = socket(AF_INET, SOCK_STREAM, 0);
  if (m_sd < 0)
    throw Connection_error{"socket()", errno};

  sockaddr_in addr;
  addr.sin_addr.s_addr = inet_addr(address);
  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);

  if (connect(m_sd, (sockaddr*)&addr, sizeof(addr)) < 0)
    throw Connection_error{"connect()", errno};
}

void Connection::send_identity(const char* player_name)
{
  int length = static_cast<int>(strlen(player_name)) + 1;
  send(length);
  send(length, player_name);
}

void Connection::recv(const ssize_t size, char* data) const
{
  ssize_t n = 0;
  ssize_t bytes_left = size;

  errno = 0;
  while ((n = read(m_sd, data, bytes_left)) > 0) {
    data += n;
    bytes_left -= n;
  }

  if (errno != 0)
    throw Connection_error{"recv()", errno};
}

void Connection::send(const ssize_t size, const char* data) const
{
  ssize_t n = 0;
  ssize_t bytes_left = size;

  errno = 0;
  while ((n = write(m_sd, data, bytes_left)) > 0) {
    data += n;
    bytes_left -= n;
  }

  if (errno != 0)
    throw Connection_error{"send()", errno};
}

} // namespace cl

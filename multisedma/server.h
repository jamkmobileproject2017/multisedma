#ifndef SERVER_H
#define SERVER_H

#include "lobbymodel.h"
#include "player.h"
#include "serverconnection.h"
#include <QObject>
#include <map>
#include <memory>
#include <thread>
#include <vector>

class Server : public QObject {
  Q_OBJECT

  Q_PROPERTY(int players READ players WRITE setPlayers)

  int m_players;
  QString m_sessionName;
  QString m_password;

  std::unique_ptr<sv::Connection> m_conn;
  std::thread m_thread;
  LobbyModel* m_lobbyModel;

  std::map<int, Player_data> m_player_data;
  std::vector<Player_identity>::const_iterator
      m_currentPlayer; // Current player to lay a card
  std::vector<Player_identity>::const_iterator
      m_tokenPlayer; // Killed the token card
  std::vector<Player_identity>::const_iterator
      m_origin_player; // Starts the round
  std::vector<LaidCard> m_laidCards;

public:
  Server();

  void hostGame();

  sv::Connection* connection() const { return m_conn.get(); }
  LobbyModel* lobbyModel() const { return m_lobbyModel; }

  int players() const { return m_players; }
  void setPlayers(int n) { m_players = n; }

signals:
  void playerConnected(QString name);
  void serverReady();

private:
  // Sends and receives bool{true} to/from every player
  void synchronize();

  void playGame();
  void playRound();
  void dealCards(Deck& d);

  void broadcastIdentities();
  void sendPlayerData();
  void sendEnemyData();
  void broadcastCurrentPlayer();
  void broadcastMessageType(MessageType mt);
  void broadcastScoreResult(int score);
  void broadcastDummyRound();
  void sendDummyRound(const Player_identity& id);

  void circPlayer();
  LaidCard layCard();

  bool hasKillingCard(Card token_card);
  void sendKillingCards(Card tokenCard);

  bool playEndOfRound(LaidCard token_card, int& score);
  void playRegularRound(int score, LaidCard token_card);
  void finishRound(int score);
};

#endif // SERVER_H

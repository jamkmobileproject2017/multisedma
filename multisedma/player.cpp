#include "player.h"
#include <cstring>

Player_identity::Player_identity() : id{-1} { memset(name, 0, sizeof(name)); }

Player_identity::Player_identity(int player_id, const char* player_name)
    : id{player_id}
{
  strncpy(name, player_name, max_name_length);
  name[max_name_length] = '\0';
}

Player_identity::Player_identity(const Player_identity& pi) noexcept : id{pi.id}
{
  strncpy(name, pi.name, max_name_length);
}

Player_identity& Player_identity::operator=(const Player_identity& pi) noexcept
{
  if (this != &pi) {
    id = pi.id;
    strncpy(name, pi.name, max_name_length);
  }

  return *this;
}

bool Player_identity::operator=(const Player_identity& rhs) const
{
  return id == rhs.id;
}

bool Player_identity::operator!=(const Player_identity& rhs) const
{
  return !operator=(rhs);
}

Player_data::Player_data() : score{0}, at_hand{0}
{
  for (Card& c : cards)
    c = Card::none;
}

void Player_data::layCard(const LaidCard& c) { cards[c.index] = Card::none; }

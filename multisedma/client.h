#ifndef CLIENT_H
#define CLIENT_H

#include "clientconnection.h"
#include <QMap>
#include <QObject>
#include <future>
#include <memory>
#include <thread>
#include <vector>

class Client : public QObject {
  Q_OBJECT

  Q_PROPERTY(QString address READ address WRITE setAddress)
  Q_PROPERTY(int port READ port WRITE setPort)
  Q_PROPERTY(QString playerName READ playerName WRITE setPlayerName)

  std::unique_ptr<cl::Connection> m_conn;
  std::thread m_thread;
  std::promise<void> m_barrier;

  QString m_address;
  int m_port;
  QString m_playerName;
  int m_playerID;
  Player_data m_playerData;

  std::promise<LaidCard> m_laidCard;
  std::promise<bool> m_passRound;

  QMap<int, QString> m_identities;
  QStringList m_enemyNames;
  QList<int> m_enemyIDs;
  QMap<int, int> m_playerScores;

public:
  Client() : QObject{nullptr} {}

  void gameInitSync();

  // synchronization needed
  cl::Connection* connection() const { return m_conn.get(); }

  void joinGame();

  QString address() const { return m_address; }
  void setAddress(QString addr) { m_address = addr; }

  int port() const { return m_port; }
  void setPort(int port) { m_port = port; }

  QString playerName() const { return m_playerName; }
  void setPlayerName(QString name) { m_playerName = name; }

  const QMap<int, QString>& identities() const { return m_identities; }
  const QStringList& playerNames() const { return m_enemyNames; }
  const QList<int>& playerIDs() const;
  int playerScore(int id) const { return m_playerScores[id]; }
  int id() const { return m_playerID; }

  void layCard(int index);
  void passRound(bool pass);

signals:
  // Emitted when the player joins the game
  void gameJoined();

  // Emitted when all players have joined
  void gameReady();

  // Deal cards at hand
  void dealCards(QStringList cards, QList<bool> enemyCards);

  // Player is asked to lay a card
  void cardRequest();

  // Emitted when it's [player ID]'s turn
  void playersTurn(int id);

  // Emitted when enemy player lays a card
  void playerLaidCard(int id, int index, QString card);

  // Emitted when a play round has ended
  void endOfRound();

  // Emitted when the round has ended, informs about the winner
  void roundWon(int id, int score);

  // Emitted when the player is asked to either pass or kill the round
  void playerPassDecision(QList<bool> killCards);

  // Emitted when the game ends with the winner's ID
  void gameEnded(int winnerID);

private:
  void synchronize();
  void recvIdentities();
  void recvPlayerData();
  QList<bool> recvKillCards();

  void playGame();
  void playRound();

  LaidCard waitForCard();
  bool waitForPassRound(const QList<bool>& killCards);
  void servePlayer();
};

#endif // CLIENT_H

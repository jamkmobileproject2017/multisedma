import QtQuick 2.0

MainMenuForm {
    btnJoin.onClicked: {
        ActivityManager.goToActivity("qrc:/ScrJoinGame.qml")
    }

    btnHost.onClicked: {
        ActivityManager.goToActivity("qrc:/ScrHostGame.qml")
    }
}

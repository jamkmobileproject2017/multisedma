import QtQuick 2.0

Item{
    id: card_parent
    property alias card_image: card_image
    property int anch_anim_dur: 300
    z: cur_index
    signal reset
    signal trigger(int card_index, string card_name)
    signal deal(var is_card_arr)
    signal end()
    anchors.fill: parent

    onDeal: {
        if(!is_card_arr[cur_index]){
            card_parent.opacity = 0
        }
    }

    onEnd: {
        card_parent.visible = false
    }

    Image {
        id: card_image
        width: card_width
        height: card_height
        source: game_table.getCardPath("backSide")
        rotation: -hand_rotation_start - rotation_increment * cur_index

        anchors {
            horizontalCenter: parent.horizontalCenter
            horizontalCenterOffset: hand_offset_start + offset_increment * cur_index

            top: parent.top
            topMargin: 0
        }
    }
    states: [
        State {
            name: "reanchored"
            PropertyChanges {
                target: card_image
                anchors{
                    horizontalCenterOffset: card_width/2
                    verticalCenterOffset:0
                }
                rotation:Math.random() * (10 - (-10)) + (-10)
                width:player_card_width
                height:player_card_height
            }
            AnchorChanges{
                target: card_image

                anchors{
                    top: undefined
                    left: undefined
                    right: undefined
                    bottom: undefined
                }
                anchors.verticalCenter: card_parent.verticalCenter
                anchors.horizontalCenter: card_parent.horizontalCenter
            }
        }
    ]

    transitions: Transition{
        AnchorAnimation{
            duration: anch_anim_dur
        }
    }

    onTrigger: {
        anch_anim_dur = 300
        card_parent.state = "reanchored";
        z = z_index++;
        card_image.source = game_table.getCardPath(card_name)
    }

    onReset: {
        anch_anim_dur = 20
        card_parent.state = ""
        card_image.source = game_table.getCardPath("backSide")
        z: cur_index
    }


}


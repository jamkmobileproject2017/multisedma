pragma Singleton
import QtQuick 2.0
//Item {
  //  id: root

QtObject {
    property color textColor: "white"

    property int smallTextFontSize: 18
    property int textFontSize: 20
    property int titleFontSize: 27

    property string fontFamilyName: "Super Mario 256"
}
//}

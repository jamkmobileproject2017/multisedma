#ifndef PLAYER_H
#define PLAYER_H

#include "card.h"

struct Player_identity {
  constexpr static int max_name_length = 19;

  int id;
  char name[max_name_length + 1];

  Player_identity();

  Player_identity(int player_id, const char* player_name);
  Player_identity(const Player_identity& pi) noexcept;
  Player_identity& operator=(const Player_identity& pi) noexcept;

  bool operator=(const Player_identity& rhs) const;
  bool operator!=(const Player_identity& rhs) const;
};

struct Player_data {
  constexpr static int max_cards_at_hand = 4;

  unsigned score;
  unsigned char at_hand;
  Card cards[max_cards_at_hand];

  Player_data();
  void layCard(const LaidCard& c);
};

enum class MessageType {
  dealCards,
  play,
  kill,
  finish,
  endPlayRound,
  endGame,
  none
};

struct ScoreResult {
  int id;
  int score;

  ScoreResult() : id{-1}, score{-1} {}
};

#endif // PLAYER_H
